const axios = require("axios");

const url = `https://mocki.io/v1/d4867d8b-b5d5-4a48-a4ab-79131b5809b8`;
const url1 = `https://reqres.in/api/users?page=2`;
const url2 = `https://jsonplaceholder.typicode.com/posts/1/comments`;
const url3 = `https://jsonplaceholder.typicode.com/posts/1`;

// promise
axios
  .get(url)
  .then((response) => {
    console.log(response.data);
  })
  .catch((error) => {
    console.error(error);
  });

axios
  .get(url1)
  .then((response) => {
    console.log(response.data);
  })
  .catch((error) => {
    console.error(error);
  });

axios
  .get(url2)
  .then((response) => {
    console.log(response.data);
  })
  .catch((error) => {
    console.error(error);
  });

axios
  .get(url3)
  .then((response) => {
    console.log(response.data);
  })
  .catch((error) => {
    console.error(error);
  });

//   async await
// async function fetch() {
//   try {
//     const response = await axios.get(url);
//     const response1 = await axios.get(url1);
//     const response2 = await axios.get(url2);
//     const response3 = await axios.get(url3);

//     // console.log(response.data);
//     console.log(response.data, response1.data, response2.data, response3.data);
//   } catch (error) {
//     console.error(error);
//   }
// }
// fetch();
