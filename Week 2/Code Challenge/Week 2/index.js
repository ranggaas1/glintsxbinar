const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
// Code Here
  let i, j;
  let len = data.length
  let isSwapped = true
  for(i = 0; i < len; i++){
    for(j = 0; j < len; j++){
        if(arr[j] > arr[j + 1]){
          let temp = arr[j]
          arr[j] = arr[j+1];
          arr[j+1] = temp;
          isSwapped = true;        
        }


      }
    if (!isSwapped){
      break
    }    
  console.log(data);
  return data
}

// Should return array
function sortDecending(data) {
  // Code Here
  var i, j;
  var len = data.length;
    
  var isSwapped = false;
    
  for(i =0; i < len; i++){
      
    isSwapped = false;
      
    for(j = 0; j < len; j++){
        if(data[j] < data[j + 1]){
          var temp = data[j]
          data[j] = data[j+1];
          data[j+1] = temp;
          isSwapped = true;
        }
    }
      
    // IF no two elements were swapped by inner loop, then break 
      
    if(!isSwapped){
      break;
    }
  }

    
  // Print the dataay
  console.log(data)
  return (data)
}


// // DON'T CHANGE
// test(sortAscending, sortDecending, data);

// console.log (data)
