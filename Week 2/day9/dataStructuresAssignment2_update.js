let status = "positive";
console.log(status);

// positif ada Bapak, Mas
// negatif ada Adik
// suspect ada Ibu, Kakak
// otg ada Ponakan, Mertua
// dead Sepupu
// sehat Grandma, Grandpa

switch (status) {
  case "positive":
    console.log("Bapak, Mas");
    break;
  case "suspect":
    console.log("Ibu, Kakak");
    break;
  case "negatif":
    console.log("Adik");
    break;
  case "otg":
    console.log("Ponakan, Mertua");
    break;
  case "dead":
    console.log("Sepupu");
    break;
  default:
    console.log("Grandpa dan Grandma sehat semua");
    break;
}
