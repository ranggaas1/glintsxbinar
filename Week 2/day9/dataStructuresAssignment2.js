let status = "suspect";
console.log(status);

// positif ada Bapak, Mas, Ponakan
// negatif ada Adik, Mertua
// suspect ada Ibu, Kakak, Sepupu
// sehat Grandma, Grandpa

switch (status) {
  case "positive":
    console.log("Bapak, Mas, Ponakan");
    break;
  case "suspect":
    console.log("Ibu, Kakak, Sepupu");
    break;
  case "negatif":
    console.log("Adik, Mertua");
    break;

  default:
    console.log("Grandpa dan Grandma sehat semua");
    break;
}
