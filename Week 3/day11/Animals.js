// Make class Animals
class Animals {
  // Instance property
  constructor(name, country) {
    this.name = name;
    this.country = country;
  }

  // Instance function
  mascot2010() {
    return `${this.type()}
${this.name} was official mascot FIFA World Cup 2010`;
  }

  mascot2014() {
    return `${this.type()}
${this.name} was official mascot FIFA World Cup 2014`;
  }

  type() {
    return `${this.name} is unique character and`;
  }
}

// Static method
Animals.address = function () {
  return `Only artificial animal character!`;
};

module.exports = Animals;
