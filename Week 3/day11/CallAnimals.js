const Animals = require("./Animals");
const Brazil = require("./Brazil");
const SouthAfrica = require("./SouthAfrica");

// make object of Animals class
let armadillo = new Animals("Fuleco the Armadillo", "Brazil");
console.log(armadillo);

let leopard = new Animals("Zakumi the Leopard", "South Africa");
console.log(leopard);

console.log(armadillo.mascot2014());
console.log(leopard.mascot2010());
console.log(Animals.address());
