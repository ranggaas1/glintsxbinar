const Animals = require("./Animals");

class SouthAfrica extends Animals {
  constructor(name, country) {
    super(name, "SouthAfrica");
    this.name = name;
  }
}

module.exports = SouthAfrica;
