const Animals = require("./Animals");

class Brazil extends Animals {
  constructor(name, country) {
    super(name, "Brazil");
    this.name = name;
  }
}

module.exports = Brazil;
