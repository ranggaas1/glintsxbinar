const students = require("../models/students.json");

class HelloController {
  getAllStudents(req, res) {
    try {
      res.status(200).json({
        data: students,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  getOneStudent(req, res) {
    try {
      const student = students.filter(
        (student) => student.id === eval(req.params.id)
      );

      res.status(200).json({
        data: student,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  createStudent(req, res) {
    try {
      students.push(req.body);

      res.status(201).json({
        data: req.body,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  deleteStudent(req, res, next) {
    const studentIndex = getIndexById(req.params.id, students);
    if (studentIndex !== -1) {
      students.splice(studentIndex, 1);
      res.status(204).json({
        data: req.body,
      });
    } else {
      res.status(404).json({
        message: error.message,
      });
    }
  }

  updateStudent(req, res) {
    try {
      let student = await students.findById(req.params.id);
      if (!student) {
        return res.status(202).json({
          data: req.body,
        });
      } else {
        let updateStudent1 = await students.findByIdAndUpdate(
          req.params.id,
          req.body,
          {
            new: true,
            runVaidator: true,
          }
        );
      }
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  // deleteStudent(req, res) {
  //   if (req.res.id) {
  //     console.log("Deleting id:" + req.query.id);
  //     students.deleteId(req.query.id);
  //     res.status(200).json({
  //       data: req.body,
  //     });
  //   } else {
  //     res.status(400).json({
  //       error: message.error,
  //     });
  //   }
}

module.exports = new HelloController();
