const express = require("express");

const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const helloRoutes = require("./routes/helloRoutes");

app.use("/ranggaas", helloRoutes);

app.listen(3001, () => console.log("port 3001"));
