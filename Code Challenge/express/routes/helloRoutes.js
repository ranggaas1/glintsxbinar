const express = require("express");

const {
  getAllStudents,
  getOneStudent,
  createStudent,
  deleteStudent,
} = require("../controllers/helloControllers");

const router = express.Router();

router.get("/", getAllStudents);

router.get("/:id", getOneStudent);

router.post("/", createStudent);

router.put("/:id", updateStudent);

router.delete("/:id", deleteStudent);

module.exports = router;
